const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TrackSchema = new Schema ({
  title: {
    type: String
  },
  album: {
    type: Schema.Types.ObjectId, ref: 'Album'
  },
  duration: {
    type: Number
  }
});

const Track = mongoose.model('Track', TrackSchema);
module.exports = Track;