const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ArtistSchema = new Schema({
  title: {
    type: String,
    require: true,
  },
  picture: {
    type: String
  },
  info: {
    type: String
  }
});

const Artist = mongoose.model('Artist', ArtistSchema);
module.exports = Artist;