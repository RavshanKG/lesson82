const express = require('express');

const Track = require('../models/Track');
const Album = require('../models/Album');
const Artist = require('../models/Artist');

const createRouter = () => {
  const router = express.Router();

  router.get('/', (req, res) => {
    if (req.query.album) {
      Track.find({album: req.query.album})
        .then(results => res.send(results))
        .catch(() => res.status(404).send({error: 'Album not found'}));
    }

    else Track.find()
        .then(results => res.send(results))
        .catch(() => res.sendStatus(500));
  })

  router.post('/', (req, res) => {
    const track = new Track(req.body);

    track.save()
      .then(track => res.send(track))
      .catch(() => res.status(400).send({err: 'error occured'}))

  })
  return router;
}

module.exports = createRouter;

