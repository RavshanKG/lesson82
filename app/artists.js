const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Artist = require('../models/Artist');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});


const createRouter = () => {
  const router = express.Router();

  router.get('/', (req, res) => {
    Artist.find()
      .then(artists => res.send(artists))
      .catch(err => res.status(404).send({error: err}));
  });

  router.get('/:id', (req, res) => {
    Artist.findById(req.params.id)
      .then(artists => res.send(artists))
      .catch(err => res.status(404).send({error: "There is no artists you are searching for"}));
  });

  router.post('/', upload.single('picture'),  (req, res) => {

    const artistData = req.body;

    if (req.file) {
      artistData.picture = req.file.filename;
    } else {
      artistData.picture = null;
    }

    const artist = new Artist(artistData);

    artist.save()
      .then(artist => res.send(artist))
      .catch(error => res.status(400).send(error))
  });
  return router
};

module.exports = createRouter;