const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Album = require('../models/Album');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const createRouter = () => {
  const router = express.Router();

  router.get('/', (req, res) => {
    Album.find().populate('performer')
      .then(albums => res.send(albums))
      .catch(err => res.status(404).send({error: err}))
  });

  router.post('/', upload.single('image'),  (req, res) => {

    const albumData = req.body;

    if (req.file) {
      albumData.image = req.file.filename;
    } else {
      albumData.image = null;
    }

    const album = new Album(albumData);

    album.save()
      .then(artist => res.send(artist))
      .catch(error => res.status(400).send(error))
  });

  return router;

};

module.exports = createRouter;