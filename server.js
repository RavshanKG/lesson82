const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');

const artists = require('./app/artists');
const albums = require('./app/albums');
const tracks = require('./app/tracks');

const app = express();

const port = 8000;

app.use(express.json());

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;


db.once('open', () => {
  console.log("mongoose connected");

  app.use('/artists', artists());
  app.use('/albums', albums());
  app.use('/tracks', tracks());

  app.listen(port, () => {
    console.log('Server started on 8000 port');
  })
});



